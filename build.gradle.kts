plugins {
    kotlin("jvm") version "1.9.23"
}

group = "org.codeberg.sunbeam.snake"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:+")
    testImplementation("org.jetbrains.kotlin:kotlin-test")
}

kotlin {
    jvmToolchain(17)
}

val fatJar = task("fatJar", type = Jar::class) {
    manifest {
        attributes["Implementation-Version"] = version
        attributes["Main-Class"] = "org.codeberg.sunbeam.snake.MainKt"
    }

    from(configurations.runtimeClasspath.get().map { if (it.isDirectory) it else zipTree(it) })
    duplicatesStrategy = DuplicatesStrategy.EXCLUDE
    with(tasks["jar"] as CopySpec)
}

tasks {
    "build" {
        dependsOn(fatJar)
    }
}

tasks.test {
    useJUnitPlatform()
}
