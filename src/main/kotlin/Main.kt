package org.codeberg.sunbeam.snake

import kotlinx.coroutines.*
import kotlin.system.exitProcess


var currentDirection = Arrow.RIGHT

const val esc = 0x1B.toChar()

const val snake = "$esc[1;31m#"
const val food = "$esc[1;32m@"

const val mapSize = 25

/**
 * Matrix of Int that looks like this:
 * ```
 * [
 *  [0, 0, 0, 0, 0],
 *  [0, 2, 3, 4, 0],
 *  [0, 1, 0, 5, 0],
 *  [0, 0, 0, 6, 0],
 *  [0, 0, 0, 0, 0],
 * ]
 * ```
 *
 * for snake pose like this:
 * ```
 * .....
 * .###.
 * .#.#.
 * ...#.
 * .....
 * ```
 *
 * Where 1 is a head and the greatest integer is a tail
 * */
var snakeMap: MutableList<MutableList<Int>> = mutableListOf()
var foodLocation = Pair(5, 5)

fun length() = snakeMap.maxOf { it.max() }

fun exit() {
    Runtime.getRuntime().exec(
        arrayOf("/bin/sh", "-c", "stty cooked </dev/tty")
    ).waitFor()

    exitProcess(0)
}

fun main() {
    val row  = (0 until mapSize).map {  0  }.toMutableList()
    snakeMap = (0 until mapSize).map { row }.toMutableList()
    snakeMap[0][0] = 1

    foodLocation = Pair(
        (0 until mapSize-1).random(),
        (0 until mapSize-1).random()
    )

    Runtime.getRuntime().exec(
        arrayOf("/bin/sh", "-c", "stty raw </dev/tty")
    ).waitFor()

    runBlocking {
        while (true) {
            var key = 0
            val buffer = ByteArray(256)

            launch(Dispatchers.Default) {
                key = withContext(Dispatchers.IO) {
                    if (System.`in`.available() == 0) return@withContext 0

                    System.`in`.read(buffer)

                    // Arrow keys have keycodes 65-68, we are finding it
                    (buffer.find { (65..68).contains(it) } ?: 0).hashCode()
                }
            }.join()

            if (key == -1) exit()

            val arrow = Arrow.entries.find { it.keyCode == key }

            if (arrow != null && !arrow.isBackwardOf(currentDirection))
                currentDirection = arrow

            renderLevel()

            Thread.sleep(
                // Initial cool down is 1000 ms, every eaten food will decrease it by 150 ms
                (1000 - (length() - 1) * 150)
                    // Minimum cool down is 100 ms, if cool down is < 100 ms then we should make it 100 ms
                    .let { if (it < 100) 100 else it }
                    .toLong()
            )

        }
    }

}