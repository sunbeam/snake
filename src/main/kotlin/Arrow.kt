package org.codeberg.sunbeam.snake

enum class Arrow(val keyCode: Int) {
    UP(65),
    DOWN(66),
    RIGHT(67),
    LEFT(68)
}

fun Arrow.isBackwardOf(arrow: Arrow) =
    if (keyCode % 2 == 0) { keyCode - 1 }
    else { keyCode + 1 } == arrow.keyCode