package org.codeberg.sunbeam.snake

fun renderLevel() {
    print("$esc[0;0f")

    val snakeHeadLocationY =
        snakeMap.indexOfFirst { it.contains(1) }.let { if (it == -1) 0 else it }

    val snakeHeadLocationX =
        snakeMap[snakeHeadLocationY].indexOfFirst { it==1 }.let { if (it == -1) 0 else it }


    val targetY = when (currentDirection) {
        Arrow.DOWN -> snakeHeadLocationY+1
        Arrow.UP -> snakeHeadLocationY-1
        else -> snakeHeadLocationY
    }

    val targetX = when (currentDirection) {
        Arrow.LEFT -> snakeHeadLocationX-1
        Arrow.RIGHT -> snakeHeadLocationX+1
        else -> snakeHeadLocationX
    }

    if (
        // Snake reached a wall
        targetX < 0
        || targetX >= mapSize

        || targetY < 0
        || targetY >= mapSize

        // Snake touched itself
        || snakeMap[targetY][targetX] != 0
    ) {
        println("$esc[0mGame over. Your score: ${length()}")
        exit()
    }

    if (snakeMap[targetY][targetX] == 1) return

    val len = length()
    val gotFood = snakeMap[foodLocation.second][foodLocation.first] > 0
    if (gotFood) foodLocation = Pair(
        (0 until mapSize-1).random(),
        (0 until mapSize-1).random()
    )

    snakeMap = snakeMap.mapIndexed y@ { y, row -> row.mapIndexed x@ { x, column ->
        if (x == targetX && y == targetY) return@x 1

        return@x when (column) {
            len -> {
                if (gotFood)
                    /* If snake ate fruit then add 1 to tail number in snake map, so snake will grow
                    *  If snake didn't eat fruit then make number zero, so tail will move
                    * */
                    column + 1 else 0
            }

            0 -> 0

            // Add 1 to every snake cell unless this is tail
            else -> column+1
        }
    }.toMutableList() }.toMutableList()

    snakeMap.forEachIndexed { y, row ->
        row.forEachIndexed { x, column ->
            print(
                if (column > 0) snake
                else if (foodLocation == Pair(x, y)) food
                else "$esc[0m "
            )
        }
        print("$esc[${y+2};0f")
    }
}